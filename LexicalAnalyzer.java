
/**
 * A lexical analyzer system for simple arithmetic expressions
 * @author Geanina Tambaliuc
 *
 */



public class LexicalAnalyzer {
	
	//variables
	static int charClass;
	static char[] lexeme= new char[100];
	static char nextChar;
	static int lexLen;
	static int token;
	static int nextToken;
	static String lexemeString;
	static int i=0;
	
	//Character classes
	final static int LETTER=0;
	final static int DIGIT=1;
	final static int UNKNOWN=99;
	
	//Token codes
	final static int INT_LIT=10;
	final static int IDENT=11;
	final static int ASSIGN_OP=20;
	final static int ADD_OP=21;
	final static int SUB_OP=22;
	final static int MULT_OP=23;
	final static int DIV_OP=24;
	final static int LEFT_PAREN=25;
	final static int RIGHT_PAREN=26;
	private static final int EOE = 100; //end of expression
	
	/**
	 * Lookup operators and parentheses
	 * @param ch
	 * @return the token
	 */
	public static int lookup(char ch)
	{
		switch(ch){
		case '(':
			addChar();
			nextToken=LEFT_PAREN;
			break;
		case ')':
			addChar();
			nextToken=RIGHT_PAREN;
			break;
		case '+':
			addChar();
			nextToken = ADD_OP;
			break;
		case '-':
			addChar();
			nextToken = SUB_OP;
			break;
		case '*':
			addChar();
			nextToken = MULT_OP;
			break;
		case '/':
			addChar();
			nextToken = DIV_OP;
			break;
		default:
			addChar();
			nextToken = EOE;
			break;
		}
		return nextToken;
	}
	
	/**
	 * Add nextChar to lexeme
	 */
	private static void addChar()
	{
		if(lexLen <=98)
		{
			lexeme[lexLen++] = nextChar;
			lexeme[lexLen]=0;
			
		}
		else
			System.out.println("Error-lexeme is too long \n");
	}
	
	
	/**
	 * Get the next character of input and determine its character class
	 */
	private static void getChar()
	{
		String expression="(sum + 47) / total";
		char[] characters=expression.toCharArray();
		
		if(i<characters.length)
				{
				nextChar=characters[i];
				if(isalpha(nextChar))
					{ charClass=LETTER; i++;}
				else if(isdigit(nextChar))
					{ charClass=DIGIT; i++;}
				else {charClass=UNKNOWN; i++;}
				}
		else charClass=EOE;
	}
	
	/**
	 * Checks if a character is a digit
	 * @param nextChar2
	 * @return true/false
	 */
	private static boolean isdigit(char nextChar2) {
		return Character.isDigit(nextChar2);
	}

	/**
	 * Checks if a character is a letter
	 * @param nextChar2
	 * @return true/false
	 */
	private static boolean isalpha(char nextChar2) {
		 return Character.isLetter(nextChar2);
	}

	/**
	 * Call getChar until it return a non-whitespace character
	 */
	private static void getNonBlank()
	{
		while(isspace(nextChar))
		getChar();
	}
	
	
	/**
	 * Checks if a character is a space
	 * @param nextChar2
	 * @return true/false
	 */
	@SuppressWarnings("deprecation")
	private static boolean isspace(char nextChar2) {
		return Character.isSpace(nextChar2);
	}

	/** Analyzer for arithmetic expressions
	 * 
	 */
	private static int lex()
	{
		lexLen = 0;
		getNonBlank();
		switch (charClass) {
		//Parse identifiers 
			case LETTER:
				addChar();
				getChar();
				while (charClass == LETTER || charClass == DIGIT) {
					addChar();
					getChar();
				}
				nextToken = IDENT;
				break;
		//Parse integer literals 
			case DIGIT:
				addChar();
				getChar();
				while (charClass == DIGIT) {
					addChar();
					getChar();
				}
				nextToken = INT_LIT;
				break;
		//Parentheses and operators 
			case UNKNOWN:
				lookup(nextChar);
				getChar();
				lexeme[2]=0;
				break;
		//End of Expression
			case EOE:
				nextToken = EOE;
				lexeme[0] = 'E';
				lexeme[1] = 'O';
				lexeme[2] = 'E';
				lexeme[3] = 0;
				lexeme[4] = 0;
				break;
		} /* End of switch */
		lexemeString=String.valueOf(lexeme);
		System.out.println("Next token is:"+ nextToken+", Next lexeme is "+lexemeString+"\n");
		return nextToken;
	}
	
	
	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {
		getChar();
		do
		{
			lex();
		}while(nextToken!=EOE);
	}
}
